package com.albassami.logistics.network.ApiManager;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CustomRestClient {
    private static OkHttpClient client = new OkHttpClient().newBuilder().addInterceptor(chain -> {
        Request original = chain.request();
        Request request = original.newBuilder()
                .header("Accept", "application/x-www-form-urlencoded")
                .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                .method(original.method(), original.body())
                .build();
        return chain.proceed(request);
    }).build();

    private static synchronized Retrofit getRetrofitClient() {

        return new Retrofit.Builder()
                .baseUrl(APIConsts.Urls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public static APIInterface getApiService() {
        return getRetrofitClient().create(APIInterface.class);
    }
}
