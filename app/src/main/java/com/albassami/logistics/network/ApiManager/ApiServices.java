package com.albassami.logistics.network.ApiManager;

import com.albassami.logistics.Utils.Const;
import com.albassami.logistics.dto.response.AuthTokenResponse;
import com.albassami.logistics.dto.response.CarsDOBResponse;
import com.albassami.logistics.dto.response.CreateOrderResponse;
import com.albassami.logistics.dto.response.GetPriceDataResponse;
import com.albassami.logistics.dto.response.PriceResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

import static com.albassami.logistics.network.ApiManager.APIConsts.Apis.AUTH_TOKEN;
import static com.albassami.logistics.network.ApiManager.APIConsts.Apis.CREATE_CUSTOMER;
import static com.albassami.logistics.network.ApiManager.APIConsts.Apis.CREATE_ORDER;
import static com.albassami.logistics.network.ApiManager.APIConsts.Apis.GET_CARS_DOB;
import static com.albassami.logistics.network.ApiManager.APIConsts.Apis.GET_CARS_SHIP;
import static com.albassami.logistics.network.ApiManager.APIConsts.Apis.GET_PRICE;
import static com.albassami.logistics.network.ApiManager.APIConsts.Apis.PRICE_DATA;

public interface ApiServices {
    @GET(AUTH_TOKEN)
    Call<AuthTokenResponse> getAuthToken(@Query(Const.Params.LOG_IN) String login, @Query(Const.Params.PASSWORD_LOG) String password, @Query(Const.Params.DB) String db);


    @FormUrlEncoded
    @POST(CREATE_CUSTOMER)
    Call<CreateOrderResponse> createCustomer(@Header(Const.Params.ACCESS_TOKEN) String token, @Field(Const.Params.CUSTOMER_NATIONAL_ID) String customer_national_id,
                                          @Field(Const.Params.CUSTOMER_NAME) String customer_name,
                                          @Field(Const.Params.CUSTOMER_PHONE) String customer_phone);

    @FormUrlEncoded
    @POST(CREATE_ORDER)
    Call<CreateOrderResponse> createOrder(@Header(Const.Params.ACCESS_TOKEN) String token, @Field(Const.Params.CUSTOMER) String customer,
                                          @Field(Const.Params.TRIP_TYPE) String trip_type,
                                          @Field(Const.Params.LOC_FROM) String locFrom, @Field(Const.Params.LOC_TO) String locTo,
                                          @Field(Const.Params.SHIPMENT_TYPE) String shipmentType, @Field(Const.Params.CAR_MAKE) String carMake,
                                          @Field(Const.Params.CAR_MODEL) String carModel, @Field(Const.Params.CAR_SIZE_ORDER) String carSize,
                                          @Field(Const.Params.PLATE_NUMBER) String plateNumber, @Field(Const.Params.PLATE_TYPE) String plateType,
                                          @Field(Const.Params.CHASSES) String chasses, @Field(Const.Params.PAYMENT_METHODE) String paymentMethod);

    @GET(PRICE_DATA)
    Call<GetPriceDataResponse> getPriceData(@Header(Const.Params.ACCESS_TOKEN) String token);

    @GET(GET_PRICE)
    Call<PriceResponse> getPrice(@Header(Const.Params.ACCESS_TOKEN) String token, @Query(Const.Params.CUSTOMER_TYPE) String customer_type, @Query(Const.Params.CAR_SIZE) String car_size, @Query(Const.Params.SERVICE_TYPE) String serviceType, @Query(Const.Params.WAYPOINT_FROM) String wayPointFrom, @Query(Const.Params.WAYPOINT_TO) String wayPointTo);

    @GET(GET_CARS_DOB)
    Call<CarsDOBResponse> getDOBLicense(@Query("national_id") String national_id);

    @GET(GET_CARS_SHIP)
    Call<CarsDOBResponse> getCsrShipLicense(@Query("national_id") String national_id);

}
