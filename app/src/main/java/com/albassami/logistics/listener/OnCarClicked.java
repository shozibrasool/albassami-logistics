package com.albassami.logistics.listener;

public interface OnCarClicked {
    void onClickCar();
}
