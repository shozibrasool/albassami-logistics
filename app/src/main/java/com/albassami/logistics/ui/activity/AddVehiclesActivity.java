package com.albassami.logistics.ui.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.albassami.logistics.NewUtilsAndPref.UiUtils;
import com.albassami.logistics.NewUtilsAndPref.sharedpref.PrefKeys;
import com.albassami.logistics.NewUtilsAndPref.sharedpref.PrefUtils;
import com.albassami.logistics.R;
import com.albassami.logistics.dto.response.AddVehicleResponse;
import com.albassami.logistics.dto.response.CarMaker;
import com.albassami.logistics.dto.response.CarModel;
import com.albassami.logistics.dto.response.DeleteVehicleResponse;
import com.albassami.logistics.dto.response.GetPriceDataResponse;
import com.albassami.logistics.dto.response.GetVehiclesResponse;
import com.albassami.logistics.dto.response.VehiclesData;
import com.albassami.logistics.network.ApiManager.APIInterface;
import com.albassami.logistics.network.ApiManager.ApiError;
import com.albassami.logistics.network.ApiManager.CustomRestClient;
import com.albassami.logistics.ui.Adapter.MyVehiclesAdapter;
import com.chaos.view.PinView;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVehiclesActivity extends AppCompatActivity implements View.OnClickListener, MyVehiclesAdapter.OnDeleteClicked, MyVehiclesAdapter.OnEditClicked {
    private RecyclerView rvVehicles;
    private LinearLayoutManager linearLayoutManager;
    String[] plateTypeList = {"Saudi Plate Number", "Other"};
    PinView pinView;
    LinearLayout othersLayout, saudiLayout;
    private MyVehiclesAdapter myVehiclesAdapter;
    private Button btnAdd;
    APIInterface apiInterface;
    EditText et_piate_number, et_id_number, et_phone_number, et_owner_name, et_plate_saudi;
    GetPriceDataResponse dataResponse;
    private AutoCompleteTextView atvCarSize, atvModel, atvPlate;
    private ArrayList<VehiclesData> vehiclesDataArrayList;
    ArrayAdapter<String> modelAdapter, carSizeAdapter, plateAdapter;
    PrefUtils prefUtils;
    ArrayList<String> carSizes;
    ArrayList<String> carMakers;
    String car_maker_id, car_size_id;
    ArrayList<CarModel> carSizesList;
    ArrayList<CarMaker> carMakersList;
    String plateDigit, completePlateNo, plateType, plateTypeUpdate, modelName, modelID, plateLetter, maker_name, type_name, plateNoUpdated = "", ownerNameUpdated = "", makerNameUpdated = "", phoneUpdated = "", typeUpdated = "", idUpdated = "", vehicleId = "";
    private ImageView btnBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicles);
        inIT();
    }

    private void inIT() {
        apiInterface = CustomRestClient.getApiService();
        prefUtils = PrefUtils.getInstance(this);
        vehiclesDataArrayList = new ArrayList<>();
        rvVehicles = findViewById(R.id.rvVehicles);
        btnAdd = findViewById(R.id.btnAddVehicle);
        btnBack = findViewById(R.id.ivBack);
        btnBack.setOnClickListener(this::onClick);
        btnAdd.setOnClickListener(this::onClick);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvVehicles.setLayoutManager(linearLayoutManager);
        UiUtils.showLoadingDialog(this);
        getVehiclesData();


    }

    private void addDialogue() {
        carMakers = new ArrayList<>();
        carSizes = new ArrayList<>();
        carMakersList = new ArrayList<>();
        carSizesList = new ArrayList<>();
        Dialog dialog = new Dialog(AddVehiclesActivity.this, R.style.custom_dialogue);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialogue_add_vehicle);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        ImageView btnCancel = dialog.findViewById(R.id.btnCancel);
        pinView = dialog.findViewById(R.id.pinView);
        othersLayout = dialog.findViewById(R.id.layoutOthers);
        saudiLayout = dialog.findViewById(R.id.layoutSaudi);
        et_plate_saudi = dialog.findViewById(R.id.etPlateSaudi);
        et_id_number = dialog.findViewById(R.id.etIDNumber);
        et_owner_name = dialog.findViewById(R.id.etOwnerName);
        et_phone_number = dialog.findViewById(R.id.etPhoneNumber);
        et_piate_number = dialog.findViewById(R.id.etPiateNumber);
        atvCarSize = dialog.findViewById(R.id.atvCarType);
        atvPlate = dialog.findViewById(R.id.atvPlateType);
        atvModel = dialog.findViewById(R.id.atvModel);
        Button addBtn = dialog.findViewById(R.id.btnAdd);
        if (!(makerNameUpdated.equalsIgnoreCase(""))) {
            atvModel.setText(makerNameUpdated);
            et_id_number.setText(idUpdated);
            et_phone_number.setText(phoneUpdated);
            et_owner_name.setText(ownerNameUpdated);
            atvCarSize.setText(typeUpdated);
            atvPlate.setText(plateTypeUpdate);
            if (plateTypeUpdate.equalsIgnoreCase("Other")) {
                othersLayout.setVisibility(View.VISIBLE);
                saudiLayout.setVisibility(View.GONE);

            } else {
                String[] splitString = plateNoUpdated.split("");
                String firstType = "";
                String secondType = "";
                for (int i = 0; i < splitString.length; i++) {
                    if (i == 0) {
                        firstType = splitString[i];
                    }
                    if (i == 1) {
                        firstType = firstType + splitString[i];
                    }
                    if (i == 2) {
                        firstType = firstType + splitString[i];
                    }
                    if (i == 3) {
                        firstType = firstType + splitString[i];
                    }
                    if (i == 4) {
                        firstType = firstType + splitString[i];

                    }
                    if (i == 5) {
                        secondType = splitString[i];
                    }
                    if (i == 6) {
                        secondType = secondType + splitString[i];
                    }
                    if (i == 7) {
                        secondType = secondType + splitString[i];
                    }
                }
                othersLayout.setVisibility(View.GONE);
                saudiLayout.setVisibility(View.VISIBLE);
                et_plate_saudi.setText(firstType);
                pinView.setText(secondType);
            }
            addBtn.setText("Edit");
        } else {
            addBtn.setText("Add");
        }

        Gson gson = new Gson();
        dataResponse = gson.fromJson(prefUtils.getStringValue(PrefKeys.JSON_OBJ, ""), GetPriceDataResponse.class);
        if (dataResponse != null && dataResponse.getData().get(0).getCarMakers() != null && dataResponse.getData().get(0).getCarMakers() != null) {
            carMakersList = dataResponse.getData().get(0).getCarMakers();
            for (int i = 0; i < dataResponse.getData().get(0).getCarMakers().size(); i++) {
                carMakers.add(dataResponse.getData().get(0).getCarMakers().get(i).getCarMakerId().getName());
            }
        }
        modelAdapter = new ArrayAdapter<String>(this, R.layout.item_drop_down, carMakers);
        plateAdapter = new ArrayAdapter<String>(this, R.layout.item_drop_down, plateTypeList);
        atvModel.setAdapter(modelAdapter);
        atvPlate.setAdapter(plateAdapter);
        atvModel.setOnItemClickListener(new MyClickListener(atvModel));
        atvCarSize.setOnItemClickListener(new MyClickListener(atvCarSize));
        atvPlate.setOnItemClickListener(new MyClickListener(atvPlate));
        atvCarSize.setDropDownWidth(250);
        atvModel.setDropDownWidth(250);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (atvCarSize.getText().toString().isEmpty()) {
                    Toast.makeText(AddVehiclesActivity.this, "select car size", Toast.LENGTH_SHORT).show();
                } else if (atvModel.getText().toString().isEmpty()) {
                    Toast.makeText(AddVehiclesActivity.this, "select car model", Toast.LENGTH_SHORT).show();
                } else if (et_phone_number.getText().toString().isEmpty()) {
                    et_phone_number.setError("enter phone number");
                } else if (atvPlate.getText().toString().isEmpty()) {
                    atvPlate.setError("select plate type");
                } else if (et_id_number.getText().toString().isEmpty()) {
                    et_id_number.setError("enter id number");
                } else if (et_owner_name.getText().toString().isEmpty()) {
                    et_owner_name.setError("enter Owner name");
                } else {
                    completePlateNo = plateDigit + plateLetter;
                    if (completePlateNo.length() != 7) {
                        completePlateNo = et_id_number.getText().toString();
                    }
                    String textBtn = btnAdd.getText().toString();
                    if (addBtn.getText().toString().equalsIgnoreCase("Edit")) {
                        dialog.cancel();
                        editVehicles(vehicleId, et_owner_name.getText().toString(), completePlateNo, et_id_number.getText().toString(), et_phone_number.getText().toString(), car_maker_id, car_size_id, maker_name, type_name);
                    } else if (addBtn.getText().toString().equalsIgnoreCase("Add")) {
                        dialog.cancel();
                        addVehicles(et_owner_name.getText().toString(), completePlateNo, et_id_number.getText().toString(), et_phone_number.getText().toString(), car_maker_id, car_size_id, modelID, modelName, plateType, maker_name, type_name);
                    }

                }
            }
        });
        atvCarSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                atvCarSize.requestFocus();
                atvCarSize.showDropDown();
            }
        });
        atvPlate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                atvPlate.requestFocus();
                atvPlate.showDropDown();
            }
        });
        atvModel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                atvModel.requestFocus();
                atvModel.showDropDown();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vehicleId = "";
                makerNameUpdated = "";
                plateTypeUpdate = "";
                typeUpdated = "";
                idUpdated = "";
                plateNoUpdated = "";
                phoneUpdated = "";
                ownerNameUpdated = "";
                dialog.cancel();
            }
        });
        pinView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String value = editable.toString();
                if (value.length() == 3) {
                    et_plate_saudi.requestFocus();
                    plateLetter = value;
                }
            }
        });
        et_plate_saudi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String value = editable.toString();
                if (value.length() == 0) {
                    pinView.requestFocus();
                } else if (value.length() == 4) {
                    plateDigit = value;
                }
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                break;
            case R.id.btnAddVehicle:
                addDialogue();
                break;
        }
    }


    private void addVehicles(String ownerName, String plateNumber, String id_number, String phoneNumber, String make_id, String type_id, String model_id, String model_name, String plate_type, String maker_name, String type_name) {
        UiUtils.showLoadingDialog(AddVehiclesActivity.this);
        Call<AddVehicleResponse> addVehicleResponseCall = apiInterface.addVehicle(ownerName, id_number, plateNumber, phoneNumber, make_id, type_id, model_id, model_name, plate_type, maker_name, type_name, String.valueOf(prefUtils.getIntValue(PrefKeys.USER_ID, 0)), prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
        addVehicleResponseCall.enqueue(new Callback<AddVehicleResponse>() {
            @Override
            public void onResponse(Call<AddVehicleResponse> call, Response<AddVehicleResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getSuccess() && response.body().getMessage() != null) {
                        Toast.makeText(AddVehiclesActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        getVehiclesData();
                    } else {
                        UiUtils.hideLoadingDialog();
                    }
                } else if (response.errorBody() != null) {
                    UiUtils.hideLoadingDialog();
                    //      ApiError message = new Gson().fromJson(response.errorBody().charStream(), ApiError.class);
                    Toast.makeText(AddVehiclesActivity.this, "error in adding vehicle", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddVehicleResponse> call, Throwable t) {
                UiUtils.hideLoadingDialog();
            }
        });
    }

    private void editVehicles(String vehicle_Id, String ownerName, String plateNumber, String id_number, String phoneNumber, String make_id, String type_id, String maker_name, String type_name) {
        UiUtils.showLoadingDialog(AddVehiclesActivity.this);
        Call<DeleteVehicleResponse> addVehicleResponseCall = apiInterface.editVehicle(vehicle_Id, ownerName, id_number, plateNumber, phoneNumber, make_id, type_id, maker_name, type_name, String.valueOf(prefUtils.getIntValue(PrefKeys.USER_ID, 0)), prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
        addVehicleResponseCall.enqueue(new Callback<DeleteVehicleResponse>() {
            @Override
            public void onResponse(Call<DeleteVehicleResponse> call, Response<DeleteVehicleResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getSuccess() && response.body().getMessage() != null) {
                        vehicleId = "";
                        makerNameUpdated = "";
                        plateTypeUpdate = "";
                        typeUpdated = "";
                        idUpdated = "";
                        plateNoUpdated = "";
                        phoneUpdated = "";
                        ownerNameUpdated = "";
                        Toast.makeText(AddVehiclesActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        getVehiclesData();
                    } else {
                        UiUtils.hideLoadingDialog();
                    }
                } else if (response.errorBody() != null) {
                    UiUtils.hideLoadingDialog();
                    //   ApiError message = new Gson().fromJson(response.errorBody().charStream(), ApiError.class);
                    Toast.makeText(AddVehiclesActivity.this, "error in edit vehicle", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DeleteVehicleResponse> call, Throwable t) {
                UiUtils.hideLoadingDialog();
            }
        });
    }

    private void getVehiclesData() {
        Call<GetVehiclesResponse> vehiclesResponseCall = apiInterface.getVehicles(String.valueOf(prefUtils.getIntValue(PrefKeys.USER_ID, 0)), prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
        vehiclesResponseCall.enqueue(new Callback<GetVehiclesResponse>() {
            @Override
            public void onResponse(Call<GetVehiclesResponse> call, Response<GetVehiclesResponse> response) {
                UiUtils.hideLoadingDialog();
                if (response.isSuccessful() && response.body() != null) {
                    vehiclesDataArrayList = new ArrayList<>();
                    if (!(response.body().getData().isEmpty())) {
                        vehiclesDataArrayList = response.body().getData();
                    }
                    myVehiclesAdapter = new MyVehiclesAdapter(AddVehiclesActivity.this, vehiclesDataArrayList, AddVehiclesActivity.this::onDelete, AddVehiclesActivity.this::onEdit);
                    rvVehicles.setAdapter(myVehiclesAdapter);
                    myVehiclesAdapter.notifyDataSetChanged();
                } else if (response.errorBody() != null) {
                    // ApiError message = new Gson().fromJson(response.errorBody().charStream(), ApiError.class);
                    Toast.makeText(AddVehiclesActivity.this, "error in getting data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetVehiclesResponse> call, Throwable t) {
                UiUtils.hideLoadingDialog();

            }
        });
    }

    private void deleteVehicle(Integer id) {
        UiUtils.showLoadingDialog(AddVehiclesActivity.this);
        Call<DeleteVehicleResponse> deleteVehicleResponseCall = apiInterface.deleteVehicles(String.valueOf(id), String.valueOf(prefUtils.getIntValue(PrefKeys.USER_ID, 0)), prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
        deleteVehicleResponseCall.enqueue(new Callback<DeleteVehicleResponse>() {
            @Override
            public void onResponse(Call<DeleteVehicleResponse> call, Response<DeleteVehicleResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getSuccess()) {
                    if (response.body().getMessage() != null) {
                        Toast.makeText(AddVehiclesActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        getVehiclesData();
                    } else {
                        UiUtils.hideLoadingDialog();
                    }
                } else if (response.errorBody() != null) {
                    UiUtils.hideLoadingDialog();
                    //  ApiError message = new Gson().fromJson(response.errorBody().charStream(), ApiError.class);
                    Toast.makeText(AddVehiclesActivity.this, "error in delete vehicle", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<DeleteVehicleResponse> call, Throwable t) {
                UiUtils.hideLoadingDialog();
            }
        });
    }

    @Override
    public void onDelete(Integer id, String name) {
        deleteVehicle(id);
    }

    @Override
    public void onEdit(Integer id, String brand, String type, String plate, String plateType, String phone, String owner, String idNumber) {
        vehicleId = id.toString();
        makerNameUpdated = brand;
        typeUpdated = type;
        idUpdated = idNumber;
        plateTypeUpdate = plateType;
        plateNoUpdated = plate;
        phoneUpdated = phone;
        ownerNameUpdated = owner;
        addDialogue();
    }


    public class MyClickListener implements AdapterView.OnItemClickListener {

        AutoCompleteTextView ac;

        public MyClickListener(AutoCompleteTextView myAc) {
            ac = myAc;
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            switch (ac.getId()) {
                case R.id.atvModel:
                    carSizesList = new ArrayList<>();
                    carSizes = new ArrayList<>();
                    for (int j = 0; j < carMakersList.size(); j++) {
                        if (atvModel.getAdapter().getItem(i).equals(carMakersList.get(i).getCarMakerId().getName())) {
                            car_maker_id = carMakersList.get(i).getCarMakerId().getId().toString();
                            maker_name = carMakersList.get(i).getCarMakerId().getName();
                            carSizesList = carMakersList.get(i).getCarModels();
                            for (int k = 0; k < carSizesList.size(); k++) {
                                carSizes.add(carSizesList.get(k).getCarModelId().getName());
                            }
                            if (!(carSizes.isEmpty())) {
                                atvCarSize.setEnabled(true);
                                atvCarSize.setClickable(true);
                                carSizeAdapter = new ArrayAdapter<String>(AddVehiclesActivity.this, R.layout.item_drop_down, carSizes);
                                atvCarSize.setAdapter(carSizeAdapter);
                            }
                            break;
                        }
                    }
                    break;

                case R.id.atvCarType:
                    for (int m = 0; m < carSizes.size(); m++) {
                        if (atvCarSize.getAdapter().getItem(i).toString().equalsIgnoreCase(carSizesList.get(m).getCarModelId().getName())) {
                            car_size_id = String.valueOf(carSizesList.get(m).getCarSize().getId());
                            modelName = atvCarSize.getAdapter().getItem(i).toString();
                            modelID = String.valueOf(carSizesList.get(m).getCarModelId().getId());
                            type_name = carSizesList.get(m).getCarSize().getName();
                            break;
                        }
                    }
                    break;

                case R.id.atvPlateType:
                    plateType = atvPlate.getAdapter().getItem(i).toString();
                    if (atvPlate.getAdapter().getItem(i).toString().equalsIgnoreCase("Other")) {
                        othersLayout.setVisibility(View.VISIBLE);
                        saudiLayout.setVisibility(View.GONE);

                    } else {
                        othersLayout.setVisibility(View.GONE);
                        saudiLayout.setVisibility(View.VISIBLE);
                    }
                    break;
            }

        }
    }
}
