package com.albassami.logistics.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputLayout;
import com.albassami.logistics.NewUtilsAndPref.AppUtils;
import com.albassami.logistics.NewUtilsAndPref.CustomText.CustomRegularEditView;
import com.albassami.logistics.NewUtilsAndPref.CustomText.CustomRegularTextView;
import com.albassami.logistics.NewUtilsAndPref.UiUtils;
import com.albassami.logistics.NewUtilsAndPref.sharedpref.PrefHelper;
import com.albassami.logistics.NewUtilsAndPref.sharedpref.PrefKeys;
import com.albassami.logistics.NewUtilsAndPref.sharedpref.PrefUtils;
import com.albassami.logistics.R;
import com.albassami.logistics.network.ApiManager.APIClient;
import com.albassami.logistics.network.ApiManager.APIConsts;
import com.albassami.logistics.network.ApiManager.APIInterface;
import com.albassami.logistics.network.ApiManager.NetworkUtils;
import com.albassami.logistics.ui.Fragment.ForgotpassFragment;
import com.albassami.logistics.ui.Fragment.SignupFragment;

import org.json.JSONObject;

import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.albassami.logistics.network.ApiManager.APIConsts.Params;
import static com.albassami.logistics.network.ApiManager.APIConsts.Params.DATA;

/**
 * Created by user on 1/4/2017.
 */

public class SignInActivity extends AppCompatActivity {
    APIInterface apiInterface;
    PrefUtils prefUtils;
    @BindView(R.id.email)
    CustomRegularEditView email;
    @BindView(R.id.password)
    CustomRegularEditView password;
    @BindView(R.id.forgotPassword)
    CustomRegularTextView forgotPassword;
    @BindView(R.id.login)
    Button login;
    @BindView(R.id.socialLogin)
    CustomRegularTextView socialLogin;
    @BindView(R.id.signUp)
    CustomRegularTextView signUp;
    @BindView(R.id.inputLayoutPassword)
    TextInputLayout inputLayoutPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        ButterKnife.bind(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        prefUtils = PrefUtils.getInstance(getApplicationContext());
        inputLayoutPassword.setHintAnimationEnabled(false);
        inputLayoutPassword.setHint("");
        password.setHint(getString(R.string.password));

        Spannable wordtoSpan = new SpannableString(getString(R.string.do_you_have_an_account_signup));
        wordtoSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)), 23, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        signUp.setText(wordtoSpan);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private boolean validateFields() {
        if (!AppUtils.isValidEmail(email.getText().toString())) {
            UiUtils.showShortToast(this, getString(R.string.email_cant_be_empty));
            return false;
        } else if (password.getText().toString().length() < 6) {
            UiUtils.showShortToast(this, getString(R.string.minimum_six_characters));
            return false;
        }
        return true;
    }


    protected void doLoginUser() {
        UiUtils.showLoadingDialog(SignInActivity.this);
        Call<String> call = apiInterface.doMannualLogin(email.getText().toString()
                , password.getText().toString()
                , APIConsts.Constants.ANDROID
                , APIConsts.Constants.MANUAL_LOGIN
                , prefUtils.getStringValue(PrefKeys.FCM_TOKEN, "")
                , TimeZone.getDefault().getID());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                UiUtils.hideLoadingDialog();
                JSONObject loginResponse = null;
                try {
                    loginResponse = new JSONObject(response.body());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (loginResponse != null) {
                    if (loginResponse.optString(APIConsts.Constants.SUCCESS).equals(APIConsts.Constants.TRUE)) {
                        JSONObject data = loginResponse.optJSONObject(DATA);
                        loginUserInDevice(data, APIConsts.Constants.MANUAL_LOGIN);
                        prefUtils.setValue(PrefKeys.IS_SOCIAL_LOGIN, false);
                    } else {
                        UiUtils.showShortToast(getApplicationContext(), loginResponse.optString(Params.ERROR));
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if(NetworkUtils.isNetworkConnected(getApplicationContext())) {
                    UiUtils.showShortToast(getApplicationContext(), getString(R.string.may_be_your_is_lost));
                }
            }
        });
    }

    public void loginUserInDevice(JSONObject data, String loginBy) {
        PrefHelper.setUserLoggedIn(this, data.optInt(Params.USER_ID)
                , data.optString(Params.TOKEN)
                , loginBy
                , data.optString(Params.EMAIL)
                , data.optString(Params.NAME)
                , data.optString(Params.NATIONAL_ID)
                , data.optString(Params.FIRSTNAME)
                , data.optString(Params.LAST_NAME)
                , data.optString(Params.PICTURE)
                , data.optString(Params.PAYMENT_MODE)
                , data.optString(Params.TIMEZONE)
                , data.optString(Params.MOBILE)
                , data.optString(Params.GENDER)
                , data.optString(Params.REFERRAL_CODE)
                , data.optString(Params.REFERRAL_BONUS));
        Intent toHome = new Intent(getApplicationContext(), MainActivity.class);
      //  toHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(toHome);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @OnClick({R.id.signUp, R.id.login, R.id.socialLogin, R.id.forgotPassword})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.signUp:
                startActivity(new Intent(getApplicationContext(), SignUpNextActivity.class));
                finish();
                break;
            case R.id.login:
                if (validateFields()) {
                    doLoginUser();
                }
                break;
            case R.id.socialLogin:
                startActivity(new Intent(getApplicationContext(), SocialLoginActivity.class));
                break;
            case R.id.forgotPassword:
                Intent forgotIntent = new Intent(getApplicationContext(), ForgotpassFragment.class);
                startActivity(forgotIntent);
                break;
        }
    }
}
