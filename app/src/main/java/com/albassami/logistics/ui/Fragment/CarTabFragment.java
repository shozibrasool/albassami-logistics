package com.albassami.logistics.ui.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.albassami.logistics.R;
import com.albassami.logistics.Utils.Const;
import com.albassami.logistics.ui.Adapter.CarTabAdapter;
import com.albassami.logistics.ui.activity.MainActivity;

import java.util.ArrayList;

public class CarTabFragment extends Fragment implements CarTabAdapter.OnItemClicked {
    RecyclerView rvTransport;
    ArrayList<String> itemList;
    LinearLayoutManager linearLayoutManager;
    CarTabAdapter carTabAdapter;
    private ImageView btnBack;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_car, container, false);
        ((MainActivity) getActivity()).hideToolbar();
        rvTransport = view.findViewById(R.id.rvTransport);
        itemList =  new ArrayList<>();
        itemList.add(getString(R.string.intercity));
        itemList.add(getString(R.string.international));
        itemList.add(getString(R.string.special_towing));
        itemList.add(getString(R.string.full_load));
        itemList.add(getString(R.string.door_to));
        btnBack = view.findViewById(R.id.btnBack);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvTransport.setLayoutManager(linearLayoutManager);
        carTabAdapter = new CarTabAdapter(getContext(),itemList,this::onClicked);
        rvTransport.setAdapter(carTabAdapter);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        return view;
    }

    @Override
    public void onClicked(String id) {
        Bundle bundle=new Bundle();
        bundle.putString(Const.PassParam.SERVICE_TYPE, id);
        CarBranchesFragment carBranchesFragment = new CarBranchesFragment();
        carBranchesFragment.setArguments(bundle);
        HomeStartFragment.getInstance().addFragment(carBranchesFragment,true, Const.BRANCHES_FRAGMENT,true);
    }
}
