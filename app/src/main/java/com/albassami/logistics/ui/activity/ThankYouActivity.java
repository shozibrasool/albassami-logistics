package com.albassami.logistics.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.albassami.logistics.R;

public class ThankYouActivity extends AppCompatActivity {
    Intent intent;
    TextView tvOrderNumber;
    String order_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        intent = getIntent();
        order_id = intent.getExtras().getString("order_id");
        tvOrderNumber = findViewById(R.id.tvOrder);
        tvOrderNumber.setText("Your Order #: " + order_id + "\n" + " has been completed");
    }
}
