package com.albassami.logistics.ui.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.albassami.logistics.R;
import com.albassami.logistics.Utils.Const;
import com.albassami.logistics.ui.activity.AddVehiclesActivity;
import com.albassami.logistics.ui.activity.HistoryActivity;
import com.albassami.logistics.ui.activity.MainActivity;
import com.albassami.logistics.ui.activity.ProfileActivity;

public class HomeStartFragment extends Fragment implements View.OnClickListener {
    LinearLayout layoutCarShip,layoutTowing,layoutOther,layoutCars,layoutProfile,layoutRides;
    private static HomeStartFragment instance = null;
    FrameLayout fragmentLayout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_home_start, container, false);
        inIt(view);
        return view;
    }

    private void inIt(View view){
        instance = this;
        layoutCars = view.findViewById(R.id.layout_my_cars);
        layoutProfile = view.findViewById(R.id.layout_profile);
        layoutRides = view.findViewById(R.id.layout_my_rides);
        layoutCarShip = view.findViewById(R.id.layoutCarShipping);
        layoutOther = view.findViewById(R.id.layoutOthers);
        layoutTowing = view.findViewById(R.id.layoutTowing);
        fragmentLayout = view.findViewById(R.id.content_fragment);
        layoutTowing.setOnClickListener(this);
        layoutOther.setOnClickListener(this);
        layoutCarShip.setOnClickListener(this);
        layoutCars.setOnClickListener(this);
        layoutProfile.setOnClickListener(this);
        layoutRides.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layoutCarShipping:
                Const.SERVICE_TYPE = Const.CarShip;
                fragmentLayout.setVisibility(View.VISIBLE);
                Fragment fragment = new CarTabFragment();
                addFragment(fragment, true, Const.CAR_FRAGMENT, true);
                break;
            case R.id.layoutTowing:
                Const.SERVICE_TYPE = Const.HomeDelivery;
                addFragment(new DoorToDoorFragment(), true, Const.DOOR_FRAGMENT, true);
                break;
            case R.id.layoutOthers:
                break;
            case R.id.layout_my_cars:
                startActivity(new Intent(getContext(), AddVehiclesActivity.class));
                break;
            case R.id.layout_profile:
                Intent i = new Intent(getContext(), ProfileActivity.class);
                startActivity(i);
                break;
            case R.id.layout_my_rides:
                startActivity(new Intent(getContext(), HistoryActivity.class).putExtra("isHistory", true));
                break;
        }
    }

    public void addFragment(Fragment fragment, boolean addToBackStack, String tag, boolean isAnimate) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (isAnimate) {
            ft.setCustomAnimations(R.anim.slide_in_right,
                    R.anim.slide_out_left, R.anim.slide_in_left,
                    R.anim.slide_out_right);
        }
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(R.id.content_fragment, fragment, tag);
        ft.commit();
    }
    public static HomeStartFragment getInstance() {
        return instance;
    }

}
