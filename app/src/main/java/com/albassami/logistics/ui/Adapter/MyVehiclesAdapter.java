package com.albassami.logistics.ui.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.albassami.logistics.NewUtilsAndPref.CustomText.CustomRegularTextView;
import com.albassami.logistics.R;
import com.albassami.logistics.dto.response.VehiclesData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 1/20/2017.
 */

public class MyVehiclesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<VehiclesData> itemsList;
    private OnDeleteClicked onDeleteClicked;
    private OnEditClicked onEditClicked;

    public MyVehiclesAdapter(Context context, ArrayList<VehiclesData> itemsList, OnDeleteClicked onDeleteClicked, OnEditClicked onEditClicked) {
        mContext = context;
        this.onDeleteClicked = onDeleteClicked;
        this.onEditClicked = onEditClicked;
        this.itemsList = itemsList;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_vehicles, null);
        BranchesViewHolder holder = new BranchesViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof BranchesViewHolder) {
            if (itemsList.get(position).getVehicleMakerName() != null && !(itemsList.get(position).getVehicleMakerName().isEmpty())){
                ((BranchesViewHolder) viewHolder).tvCarBrand.setText("Brand: " + itemsList.get(position).getVehicleMakerName());
            }
            if (itemsList.get(position).getVehicleTypeName() != null && !(itemsList.get(position).getVehicleTypeName().isEmpty())){
                ((BranchesViewHolder) viewHolder).tvCarType.setText("Type: " + itemsList.get(position).getVehicleTypeName());
            }
            if (itemsList.get(position).getOwnerName() != null && !(itemsList.get(position).getOwnerName().isEmpty())){
                ((BranchesViewHolder) viewHolder).tvOwnerName.setText("Owner: " + itemsList.get(position).getOwnerName());
            }
            if (itemsList.get(position).getIdNumber() != null && !(itemsList.get(position).getIdNumber().isEmpty())){
                ((BranchesViewHolder) viewHolder).tvIDNumber.setText("ID No.: " + itemsList.get(position).getIdNumber());
            }
            if (itemsList.get(position).getPlateNumber() != null && !(itemsList.get(position).getPlateNumber().isEmpty())){
                ((BranchesViewHolder) viewHolder).tvPlateNumber.setText("Plate No.: " + itemsList.get(position).getPlateNumber());
            }
            if (itemsList.get(position).getPhoneNumber() != null && !(itemsList.get(position).getPhoneNumber().isEmpty())){
                ((BranchesViewHolder) viewHolder).tvPhoneNumber.setText("Phone No.: " + itemsList.get(position).getPhoneNumber());
            }
            ((BranchesViewHolder) viewHolder).ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onEditClicked.onEdit(itemsList.get(position).getId(), itemsList.get(position).getVehicleMakerName(),itemsList.get(position).getVehicleTypeName(),itemsList.get(position).getPlateNumber(),itemsList.get(position).getPlateType(),itemsList.get(position).getPhoneNumber(),itemsList.get(position).getOwnerName(),itemsList.get(position).getIdNumber());
                }
            });
            ((BranchesViewHolder) viewHolder).ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onDeleteClicked.onDelete(itemsList.get(position).getId(), "");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }


    public class BranchesViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivDelete)
        ImageView ivDelete;
        @BindView(R.id.ivEdit)
        ImageView ivEdit;
        @BindView(R.id.tvCarBrand)
        CustomRegularTextView tvCarBrand;
        @BindView(R.id.tvCarType)
        CustomRegularTextView tvCarType;
        @BindView(R.id.tvPhoneNumber)
        CustomRegularTextView tvPhoneNumber;
        @BindView(R.id.tvIDNumber)
        CustomRegularTextView tvIDNumber;
        @BindView(R.id.tvOwnerName)
        CustomRegularTextView tvOwnerName;
        @BindView(R.id.tvPlateNumber)
        CustomRegularTextView tvPlateNumber;

        public BranchesViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnDeleteClicked {
        void onDelete(Integer id, String name);
    }

    public interface OnEditClicked {
        void onEdit(Integer id, String brand,String type,String plate,String plateType,String phone,String owner,String idNumber);
    }
}


