package com.albassami.logistics.ui.activity;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.albassami.logistics.NewUtilsAndPref.UiUtils;
import com.albassami.logistics.NewUtilsAndPref.sharedpref.PrefKeys;
import com.albassami.logistics.NewUtilsAndPref.sharedpref.PrefUtils;
import com.albassami.logistics.R;
import com.albassami.logistics.Utils.Const;
import com.albassami.logistics.dto.response.CreateOrderResponse;
import com.albassami.logistics.dto.response.PriceResponse;
import com.albassami.logistics.network.ApiManager.APIClient;
import com.albassami.logistics.network.ApiManager.APIConsts;
import com.albassami.logistics.network.ApiManager.APIInterface;
import com.albassami.logistics.network.ApiManager.ApiError;
import com.albassami.logistics.network.ApiManager.ApiServices;
import com.albassami.logistics.network.ApiManager.NetworkUtils;
import com.albassami.logistics.network.ApiManager.RestClient;
import com.albassami.logistics.payfort.IPaymentRequestCallBack;
import com.albassami.logistics.payfort.PayFortData;
import com.albassami.logistics.payfort.PayFortPayment;
import com.albassami.logistics.ui.Adapter.PaymentIconAdapter;
import com.albassami.logistics.ui.Fragment.FragmentOrderSummary;
import com.google.gson.Gson;
import com.payfort.fort.android.sdk.base.callbacks.FortCallBackManager;
import com.payfort.fort.android.sdk.base.callbacks.FortCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderSummaryActivity extends AppCompatActivity implements IPaymentRequestCallBack {
    String carSize, phoneNumber, receivrName, maker_id, carModelName, receiverNumber, cashCard = "", idNumber, piateNumber, carModel, serviceType, branchName, ownerName, branchId, carSizeID, carModelID, branchNameTo, branchIdTo;
    ArrayList<Integer> iconList;
    PaymentIconAdapter paymentIconAdapter;
    public FortCallBackManager fortCallback = null;
    ApiServices apiServices;
    TextView tvPhoneNumber, tvReceiverName, tvTotalCost, tvSenderName, tvSource, tvCarSize, tvDestination, tvService, tvPrice, tvCash, tvCard;
    PrefUtils prefUtils;
    String payPrice = "";
    Button btnNext;
    LinearLayout layoutReceiver, layoutReceiverNumber, layout_vehicle;
    ImageView btnBack;
    APIInterface apiInterface;

    private void initilizePayFortSDK() {
        fortCallback = FortCallback.Factory.create();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);
        inIT();

    }

    private void inIT() {
        initilizePayFortSDK();
        serviceType = getIntent().getExtras().getString(Const.PassParam.SERVICE_TYPE);
        branchName = getIntent().getExtras().getString(Const.PassParam.BRANCH_NAME);
        ownerName = getIntent().getExtras().getString(Const.PassParam.OWNER_NAME);
        idNumber = getIntent().getExtras().getString(Const.PassParam.ID_NUMBER);
        phoneNumber = getIntent().getExtras().getString(Const.PassParam.PHONE_NUMBER);
        piateNumber = getIntent().getExtras().getString(Const.PassParam.PIATE_NUMBER);
        carSize = getIntent().getExtras().getString(Const.PassParam.CAR_SIZE);
        carModel = getIntent().getExtras().getString(Const.PassParam.CAR_MODEL);
        branchId = getIntent().getExtras().getString(Const.PassParam.BRANCH_ID);
        carSizeID = getIntent().getExtras().getString(Const.PassParam.CAR_SIZE_ID);
        carModelID = getIntent().getExtras().getString(Const.PassParam.CAR_MODEL_ID);
        maker_id = getIntent().getExtras().getString(Const.PassParam.CAR_MAKER_ID);
        branchIdTo = getIntent().getExtras().getString(Const.PassParam.BRANCH_ID_TO);
        branchNameTo = getIntent().getExtras().getString(Const.PassParam.BRANCH_NAME_TO);
        receivrName = getIntent().getExtras().getString(Const.PassParam.RECEIVER_NAME);
        receiverNumber = getIntent().getExtras().getString(Const.PassParam.RECEIVER_NUMBER);
        carModelName = getIntent().getExtras().getString(Const.PassParam.CAR_MODEL_NAME);
        prefUtils = PrefUtils.getInstance(this);
        //  rvIcons = findViewById(R.id.rvIcons);
        btnBack = findViewById(R.id.btnBack);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        tvPhoneNumber = findViewById(R.id.tvPhoneNumber);
        tvCash = findViewById(R.id.tvCash);
        tvCard = findViewById(R.id.tvCard);
        tvSource = findViewById(R.id.tvSource);
        tvDestination = findViewById(R.id.tvDestination);
        tvPrice = findViewById(R.id.tvPrice);
        tvSenderName = findViewById(R.id.tvSenderName);
        tvReceiverName = findViewById(R.id.tvReceiverName);
        tvCarSize = findViewById(R.id.tvCarSize);
        tvService = findViewById(R.id.tvServiceType);
        btnNext = findViewById(R.id.btnNext);
        layoutReceiver = findViewById(R.id.layout_receiver);
        layoutReceiverNumber = findViewById(R.id.layout_receiver_mobile);
        layout_vehicle = findViewById(R.id.layout_vehicle);
        iconList = new ArrayList<>();
        iconList.add(R.drawable.viza_card);
        iconList.add(R.drawable.master_card);
        iconList.add(R.drawable.apple_pay);
//        rvIcons.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//        paymentIconAdapter = new PaymentIconAdapter(this, iconList);
//        rvIcons.setAdapter(paymentIconAdapter);
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss a");
        String currentTime = df.format(Calendar.getInstance().getTime());
        tvService.setText(serviceType);
        if (serviceType.equalsIgnoreCase(Const.DOOR_TO_DOOR)) {
            layout_vehicle.setVisibility(View.GONE);
            layoutReceiverNumber.setVisibility(View.GONE);
            layoutReceiver.setVisibility(View.GONE);
        } else {
            layout_vehicle.setVisibility(View.VISIBLE);
            layoutReceiverNumber.setVisibility(View.VISIBLE);
            layoutReceiver.setVisibility(View.VISIBLE);
            tvCarSize.setText(carSize + "-" + carModelName + "-" + piateNumber);
            tvPhoneNumber.setText(receiverNumber);
            tvReceiverName.setText(receivrName);
        }


        tvDestination.setText(branchNameTo);
        tvSource.setText(branchName);

        tvSenderName.setText(prefUtils.getStringValue(PrefKeys.USER_NAME, ""));
        onClickListners();
        getPrice();
    }

    private void onClickListners() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (prefUtils.getBooleanValue(PrefKeys.IS_LOGGED_IN, false)) {
                    if (TextUtils.isEmpty(cashCard)) {
                        Toast.makeText(OrderSummaryActivity.this, "Please select payment method", Toast.LENGTH_SHORT).show();
                    } else if (cashCard.equalsIgnoreCase("cash")) {
//                        Intent intent = new Intent(OrderSummaryActivity.this, RideLaterActivity.class);
//                        Bundle bundle = new Bundle();
//                        bundle.putString(Const.PassParam.BRANCH_NAME, branchName);
//                        bundle.putString(Const.PassParam.BRANCH_ID, branchId);
//                        bundle.putString(Const.PassParam.BRANCH_NAME_TO, branchNameTo);
//                        bundle.putString(Const.PassParam.BRANCH_ID_TO, branchIdTo);
//                        bundle.putString(Const.PassParam.SERVICE_TYPE, serviceType);
//                        bundle.putString(Const.PassParam.CAR_MODEL, carModel);
//                        bundle.putString(Const.PassParam.CAR_SIZE, carSize);
//                        bundle.putString(Const.PassParam.CAR_MODEL_ID, carModelID);
//                        bundle.putString(Const.PassParam.CAR_SIZE_ID, carSizeID);
//                        bundle.putString(Const.PassParam.ID_NUMBER, idNumber);
//                        bundle.putString(Const.PassParam.PHONE_NUMBER, phoneNumber);
//                        bundle.putString(Const.PassParam.OWNER_NAME, ownerName);
//                        bundle.putString(Const.PassParam.PIATE_NUMBER, piateNumber);
//                        bundle.putString(Const.PassParam.AGGREMENT_ID, getIntent().getExtras().getString(Const.PassParam.AGGREMENT_ID));
//                        bundle.putString(Const.PassParam.SOURCE_ADDRESS, getIntent().getExtras().getString(Const.PassParam.SOURCE_ADDRESS));
//                        bundle.putString(Const.PassParam.DEST_ADDRESS, getIntent().getExtras().getString(Const.PassParam.DEST_ADDRESS));
//                        bundle.putString(Const.PassParam.PRICE, tvPrice.getText().toString());
//                        bundle.putString(Const.PassParam.CAR_MODEL_NAME, carModelName);
//                        bundle.putString(Const.PassParam.RECEIVER_NAME, receivrName);
//                        bundle.putString(Const.PassParam.RECEIVER_NUMBER, receiverNumber);
//                        intent.putExtras(bundle);
//                        startActivity(intent);
                        // createANowRequest();
                        if (Const.SERVICE_TYPE.equalsIgnoreCase(Const.CarShip)) {
                            createCustomerNow();
                        } else if (Const.SERVICE_TYPE.equalsIgnoreCase(Const.HomeDelivery)) {
                            goTorideLater();
                            //   createANowRequest();
                        }
                    } else if (cashCard.equalsIgnoreCase("card")) {
                        requestForPayfortPayment();
                    }
                } else {
                    startActivity(new Intent(OrderSummaryActivity.this, ActivityLoginSigupOption.class));
                }
            }
        });
        tvCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GradientDrawable cardDrawable = (GradientDrawable) tvCard.getBackground().mutate();
                cardDrawable.setColor(getResources().getColor(R.color.dark_grey));
                GradientDrawable cashDrawable = (GradientDrawable) tvCash.getBackground().mutate();
                cashDrawable.setColor(getResources().getColor(R.color.et_color));
                cashCard = "card";
            }
        });
        tvCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GradientDrawable cardDrawable = (GradientDrawable) tvCash.getBackground().mutate();
                cardDrawable.setColor(getResources().getColor(R.color.dark_grey));
                GradientDrawable cashDrawable = (GradientDrawable) tvCard.getBackground().mutate();
                cashDrawable.setColor(getResources().getColor(R.color.et_color));
                cashCard = "cash";
            }
        });
    }

    private void getPrice() {
        UiUtils.showLoadingDialog(OrderSummaryActivity.this);
        apiServices = RestClient.getApiService();
        Call<PriceResponse> priceResponseCall = apiServices.getPrice(prefUtils.getStringValue(PrefKeys.AUTH_TOKEN, ""), "individual", carSizeID, "1", branchId, branchIdTo);
        priceResponseCall.enqueue(new Callback<PriceResponse>() {
            @Override
            public void onResponse(Call<PriceResponse> call, Response<PriceResponse> response) {
                UiUtils.hideLoadingDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getData() != null && !(response.body().getData().isEmpty()))
                        tvPrice.setText(String.valueOf(response.body().getData().get(0).getPrice()) + "SR");
                    payPrice = String.valueOf(response.body().getData().get(0).getPrice());
                    prefUtils.setValue(PrefKeys.PRICE, String.valueOf(response.body().getData().get(0).getPrice()) + "SR");
                } else if (response.errorBody() != null) {
                    ApiError message = new Gson().fromJson(response.errorBody().charStream(), ApiError.class);
                    Toast.makeText(OrderSummaryActivity.this, "" + message.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PriceResponse> call, Throwable t) {
                UiUtils.hideLoadingDialog();
            }
        });
    }

    private void createOrderNow(String customer_id) {
        apiServices = RestClient.getApiService();
        Call<CreateOrderResponse> orderResponseCall = apiServices.createOrder(prefUtils.getStringValue(PrefKeys.AUTH_TOKEN, ""), customer_id,
                "onway", branchId, branchIdTo, "1", carModelID, maker_id, carSizeID, piateNumber, "1", "444", "6");
        orderResponseCall.enqueue(new Callback<CreateOrderResponse>() {
            @Override
            public void onResponse(Call<CreateOrderResponse> call, Response<CreateOrderResponse> response) {
                UiUtils.hideLoadingDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getData() != null && !(response.body().getData().isEmpty())) {
                        if (response.body().getData().get(0).getOrderId() != null) {
                            Intent intent = new Intent(OrderSummaryActivity.this,ThankYouActivity.class);
                            intent.putExtra("order_id",response.body().getData().get(0).getOrderId().toString());
                            startActivity(intent);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CreateOrderResponse> call, Throwable t) {
                UiUtils.hideLoadingDialog();
            }
        });

    }

    private void createCustomerNow() {
        UiUtils.showLoadingDialog(OrderSummaryActivity.this);
        apiServices = RestClient.getApiService();
        Call<CreateOrderResponse> orderResponseCall = apiServices.createCustomer(prefUtils.getStringValue(PrefKeys.AUTH_TOKEN, ""), (prefUtils.getStringValue(PrefKeys.NATIONAL_ID, "")),
                (prefUtils.getStringValue(APIConsts.Params.NAME, "")), (prefUtils.getStringValue(APIConsts.Params.MOBILE, "")));
        orderResponseCall.enqueue(new Callback<CreateOrderResponse>() {
            @Override
            public void onResponse(Call<CreateOrderResponse> call, Response<CreateOrderResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getData() != null && !(response.body().getData().isEmpty())) {
                        if (response.body().getData().get(0).getCustomerId() != null) {
                            createOrderNow(response.body().getData().get(0).getCustomerId().toString());
                        } else {
                            UiUtils.hideLoadingDialog();
                        }
                    } else {
                        UiUtils.hideLoadingDialog();
                    }
                } else {
                    UiUtils.hideLoadingDialog();
                }
            }

            @Override
            public void onFailure(Call<CreateOrderResponse> call, Throwable t) {
                UiUtils.hideLoadingDialog();
            }
        });

    }

    protected void createANowRequest() {
        UiUtils.showLoadingDialog(OrderSummaryActivity.this);
        try {
            Call<String> call = apiInterface.createNowRequest(prefUtils.getIntValue(PrefKeys.USER_ID, 0)
                    , prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, "")
                    , ""
                    , prefUtils.getStringValue(PrefKeys.PRICE, "")
                    , Const.source_address
                    , Const.dest_address
                    , Const.stop_address != null ? Const.stop_address : ""
                    , Const.pic_latlan != null ? Const.pic_latlan.latitude : 0
                    , Const.pic_latlan != null ? Const.pic_latlan.longitude : 0
                    , Const.drop_latlan != null ? Const.drop_latlan.latitude : 0
                    , Const.drop_latlan != null ? Const.drop_latlan.longitude : 0
                    , Const.stop_latlan != null ? Const.stop_latlan.latitude : 0
                    , Const.stop_latlan != null ? Const.stop_latlan.longitude : 0
                    , Const.SERVICE_TYPE
                    , getIntent().getExtras().getString(Const.PassParam.AGGREMENT_ID)
                    , "towing"
                    , branchIdTo
                    , branchId
                    , 1
                    , ""
                    , prefUtils.getStringValue(PrefKeys.PAYMENT_MODE, "")
            );
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    JSONObject createRequestResponse = null;
                    UiUtils.hideLoadingDialog();
                    try {
                        createRequestResponse = new JSONObject(response.body());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (createRequestResponse != null) {
                        if (createRequestResponse.optString(Const.Params.SUCCESS).equals(APIConsts.Constants.TRUE)) {
                            try {
                                Toast.makeText(OrderSummaryActivity.this, createRequestResponse.getString("message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Intent intent = new Intent(OrderSummaryActivity.this, ActivityWaitingRequestAccept.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(Const.PassParam.BRANCH_NAME, branchName);
                            bundle.putString(Const.PassParam.BRANCH_ID, branchId);
                            bundle.putString(Const.PassParam.BRANCH_NAME_TO, branchNameTo);
                            bundle.putString(Const.PassParam.BRANCH_ID_TO, branchIdTo);
                            bundle.putString(Const.PassParam.SERVICE_TYPE, serviceType);
                            bundle.putString(Const.PassParam.CAR_MODEL, carModel);
                            bundle.putString(Const.PassParam.CAR_SIZE, carSize);
                            bundle.putString(Const.PassParam.CAR_MODEL_ID, carModelID);
                            bundle.putString(Const.PassParam.CAR_SIZE_ID, carSizeID);
                            bundle.putString(Const.PassParam.ID_NUMBER, idNumber);
                            bundle.putString(Const.PassParam.PHONE_NUMBER, phoneNumber);
                            bundle.putString(Const.PassParam.OWNER_NAME, ownerName);
                            bundle.putString(Const.PassParam.PIATE_NUMBER, piateNumber);
                            bundle.putString(Const.PassParam.PRICE, tvPrice.getText().toString());
                            bundle.putString(Const.PassParam.CAR_MODEL_NAME, carModelName);
                            bundle.putString(Const.PassParam.RECEIVER_NAME, receivrName);
                            bundle.putString(Const.PassParam.RECEIVER_NUMBER, receiverNumber);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        } else {
                            try {
                                if (createRequestResponse.getString(Const.Params.ERROR_MSG) != null) {
                                    Toast.makeText(OrderSummaryActivity.this, createRequestResponse.getString(Const.Params.ERROR_MSG), Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

//                            enableRequestBtn();
//                            UiUtils.showShortToast(getActivity(), createRequestResponse.optString(APIConsts.Params.ERROR));
//                            if (req_load_dialog != null && req_load_dialog.isShowing()) {
//                                req_load_dialog.dismiss();
//                                stopCheckingforstatus();
//                            }
                            cancelCreateRequest();
                            if (createRequestResponse.optInt(APIConsts.Params.ERROR_CODE) == APIConsts.ErrorCodes.WALLETEMPTY) {
                                Intent walletIntent = new Intent(OrderSummaryActivity.this, WalletAcivity.class);
                                startActivity(walletIntent);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    UiUtils.hideLoadingDialog();
                    if (NetworkUtils.isNetworkConnected(OrderSummaryActivity.this)) {
                        UiUtils.showShortToast(OrderSummaryActivity.this, getString(R.string.may_be_your_is_lost));
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void cancelCreateRequest() {
        UiUtils.showLoadingDialog(OrderSummaryActivity.this);
        Call<String> call = apiInterface.cancelRequest(prefUtils.getIntValue(PrefKeys.USER_ID, 0)
                , prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                UiUtils.hideLoadingDialog();
                JSONObject cancelResponse = null;
                try {
                    cancelResponse = new JSONObject(response.body());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                UiUtils.hideLoadingDialog();
                if (NetworkUtils.isNetworkConnected(OrderSummaryActivity.this)) {
                    UiUtils.showShortToast(OrderSummaryActivity.this, getString(R.string.may_be_your_is_lost));
                }
            }
        });
    }

    private void requestForPayfortPayment() {
        PayFortData payFortData = new PayFortData();
        if (!TextUtils.isEmpty(payPrice)) {
            payFortData.amount = String.valueOf((int) (Float.parseFloat(payPrice) * 100));// Multiplying with 100, bcz amount should not be in decimal format
            payFortData.command = PayFortPayment.PURCHASE;
            payFortData.currency = PayFortPayment.CURRENCY_TYPE;
            payFortData.customerEmail = "hamdan@albassami.com";
            payFortData.language = PayFortPayment.LANGUAGE_TYPE;
            payFortData.merchantReference = String.valueOf(System.currentTimeMillis());

            PayFortPayment payFortPayment = new PayFortPayment(this, this.fortCallback, this);
            payFortPayment.requestForPayment(payFortData);
        }
    }

    @Override
    public void onPaymentRequestResponse(int responseType, final PayFortData responseData) {
        if (responseType == PayFortPayment.RESPONSE_GET_TOKEN) {
            Toast.makeText(this, "Token not generated", Toast.LENGTH_SHORT).show();
            Log.e("onPaymentResponse", "Token not generated");
        } else if (responseType == PayFortPayment.RESPONSE_PURCHASE_CANCEL) {
            Toast.makeText(this, "Payment cancelled", Toast.LENGTH_SHORT).show();
            Log.e("onPaymentResponse", "Payment cancelled");
        } else if (responseType == PayFortPayment.RESPONSE_PURCHASE_FAILURE) {
            Toast.makeText(this, "Payment failed", Toast.LENGTH_SHORT).show();
            Log.e("onPaymentResponse", "Payment failed");
        } else {
            Toast.makeText(this, "Payment Successful", Toast.LENGTH_SHORT).show();
            if (Const.SERVICE_TYPE.equalsIgnoreCase(Const.CarShip)) {
                createCustomerNow();
            } else if (Const.SERVICE_TYPE.equalsIgnoreCase(Const.HomeDelivery)) {
                goTorideLater();
//                createANowRequest();
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PayFortPayment.RESPONSE_PURCHASE) {
            fortCallback.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void goTorideLater() {
        Intent intent = new Intent(OrderSummaryActivity.this, RideLaterActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Const.PassParam.BRANCH_NAME, branchName);
        bundle.putString(Const.PassParam.BRANCH_ID, branchId);
        bundle.putString(Const.PassParam.BRANCH_NAME_TO, branchNameTo);
        bundle.putString(Const.PassParam.BRANCH_ID_TO, branchIdTo);
        bundle.putString(Const.PassParam.SERVICE_TYPE, serviceType);
        bundle.putString(Const.PassParam.CAR_MODEL, carModel);
        bundle.putString(Const.PassParam.CAR_SIZE, carSize);
        bundle.putString(Const.PassParam.CAR_MODEL_ID, carModelID);
        bundle.putString(Const.PassParam.CAR_SIZE_ID, carSizeID);
        bundle.putString(Const.PassParam.ID_NUMBER, idNumber);
        bundle.putString(Const.PassParam.PHONE_NUMBER, phoneNumber);
        bundle.putString(Const.PassParam.OWNER_NAME, ownerName);
        bundle.putString(Const.PassParam.PIATE_NUMBER, piateNumber);
        bundle.putString(Const.PassParam.AGGREMENT_ID, getIntent().getExtras().getString(Const.PassParam.AGGREMENT_ID));
        bundle.putString(Const.PassParam.SOURCE_ADDRESS, getIntent().getExtras().getString(Const.PassParam.SOURCE_ADDRESS));
        bundle.putString(Const.PassParam.DEST_ADDRESS, getIntent().getExtras().getString(Const.PassParam.DEST_ADDRESS));
        bundle.putString(Const.PassParam.TOWING_TYPE, getIntent().getExtras().getString(Const.PassParam.TOWING_TYPE));
        bundle.putString(Const.PassParam.PRICE, tvPrice.getText().toString());
        bundle.putString(Const.PassParam.CAR_MODEL_NAME, carModelName);
        bundle.putString(Const.PassParam.RECEIVER_NAME, receivrName);
        bundle.putString(Const.PassParam.RECEIVER_NUMBER, receiverNumber);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
