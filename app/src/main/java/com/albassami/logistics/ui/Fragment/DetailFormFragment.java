package com.albassami.logistics.ui.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.albassami.logistics.NewUtilsAndPref.UiUtils;
import com.albassami.logistics.NewUtilsAndPref.sharedpref.PrefKeys;
import com.albassami.logistics.NewUtilsAndPref.sharedpref.PrefUtils;
import com.albassami.logistics.R;
import com.albassami.logistics.Utils.Const;
import com.albassami.logistics.dto.response.CarMaker;
import com.albassami.logistics.dto.response.CarModel;
import com.albassami.logistics.dto.response.GetVehiclesResponse;
import com.albassami.logistics.dto.response.VehiclesData;
import com.albassami.logistics.network.ApiManager.APIInterface;
import com.albassami.logistics.network.ApiManager.ApiError;
import com.albassami.logistics.network.ApiManager.CustomRestClient;
import com.albassami.logistics.ui.activity.OrderSummaryActivity;
import com.chaos.view.PinView;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailFormFragment extends Fragment implements View.OnClickListener {
    Button nextBtn;
    private ImageView btnBack;
    ArrayAdapter<String> nationalityModel, vehiclesModel;
    String[] nationalityList = {"Saudi", "Egyptian", "Pakistani"};
    ArrayList<String> carsList;
    private ArrayList<VehiclesData> vehiclesDataArrayList;
    ArrayList<CarModel> carSizesList;
    PrefUtils prefUtils;
    LinearLayout layoutCarDetail;
    APIInterface apiInterface;
    private AutoCompleteTextView atvCars, atvNationality;
    String service_type, branch_name, branch_id,carModelName,maker_id,receiverName,recieverNumber, user_phone_number, car_size, owner_name, id_number, user_plate_number, car_size_id, car_model_id, branch_name_to, branch_id_to;
    EditText etReceiverName, etReceiverNumber, etReceiverID,etSenderName;
    TextView etCarName,etCarModel,etCarPlate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_between_cities, container, false);
        inIT(view);
        return view;
    }

    private void inIT(View view) {
        carsList = new ArrayList<>();
        vehiclesDataArrayList = new ArrayList<>();
        apiInterface = CustomRestClient.getApiService();
        service_type = getArguments().getString(Const.PassParam.SERVICE_TYPE);
        branch_name = getArguments().getString(Const.PassParam.BRANCH_NAME);
        branch_id = getArguments().getString(Const.PassParam.BRANCH_ID);
        branch_name_to = getArguments().getString(Const.PassParam.BRANCH_NAME_TO);
        branch_id_to = getArguments().getString(Const.PassParam.BRANCH_ID_TO);
        prefUtils = PrefUtils.getInstance(getContext());
        layoutCarDetail = view.findViewById(R.id.layoutCarDetail);
        etSenderName = view.findViewById(R.id.etSenderName);
        etReceiverName = view.findViewById(R.id.etReceiverName);
        etReceiverNumber = view.findViewById(R.id.etReceiverPhone);
        etReceiverID = view.findViewById(R.id.etIDNumber);
        etCarName = view.findViewById(R.id.etCarName);
        etCarModel = view.findViewById(R.id.etCarModel);
        etCarPlate = view.findViewById(R.id.etPlateNumber);
        atvCars = view.findViewById(R.id.atvCars);
        atvNationality = view.findViewById(R.id.atvNationality);
        nextBtn = view.findViewById(R.id.btnNext);
        btnBack = view.findViewById(R.id.btnBack);
        atvNationality.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        atvCars.setOnClickListener(this);
        atvCars.setOnItemClickListener(new MyClickListener(atvCars));
        atvNationality.setOnItemClickListener(new MyClickListener(atvNationality));
        nationalityModel = new ArrayAdapter<String>(getContext(), R.layout.item_drop_down, nationalityList);
        atvNationality.setAdapter(nationalityModel);
        getVehiclesData();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
                if (etCarName.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), "select car size", Toast.LENGTH_SHORT).show();
                } else if (atvNationality.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), "select nationality", Toast.LENGTH_SHORT).show();
                } else if (etReceiverName.getText().toString().isEmpty()) {
                    etReceiverName.setError("enter receiver name");
                }else if (etReceiverNumber.getText().toString().isEmpty()) {
                    etReceiverNumber.setError("enter receiver number");
                }
                else {

                    Bundle bundle = new Bundle();
                    bundle.putString(Const.PassParam.BRANCH_NAME, branch_name);
                    bundle.putString(Const.PassParam.BRANCH_ID, branch_id);
                    bundle.putString(Const.PassParam.BRANCH_NAME_TO, branch_name_to);
                    bundle.putString(Const.PassParam.BRANCH_ID_TO, branch_id_to);
                    bundle.putString(Const.PassParam.SERVICE_TYPE, service_type);
                    bundle.putString(Const.PassParam.CAR_MODEL, etCarModel.getText().toString());
                    bundle.putString(Const.PassParam.CAR_SIZE, car_size);
                    bundle.putString(Const.PassParam.CAR_MODEL_ID, car_model_id);
                    bundle.putString(Const.PassParam.CAR_SIZE_ID, car_size_id);
                    bundle.putString(Const.PassParam.CAR_MAKER_ID, maker_id);
                    bundle.putString(Const.PassParam.ID_NUMBER, id_number);
                    bundle.putString(Const.PassParam.PHONE_NUMBER, user_phone_number);
                    bundle.putString(Const.PassParam.OWNER_NAME, owner_name);
                    bundle.putString(Const.PassParam.CAR_MODEL_NAME, carModelName);
                    bundle.putString(Const.PassParam.RECEIVER_NAME, etReceiverName.getText().toString());
                    bundle.putString(Const.PassParam.RECEIVER_NUMBER, etReceiverNumber.getText().toString());
                    bundle.putString(Const.PassParam.PIATE_NUMBER, user_plate_number);
                    Intent intent = new Intent(getContext(), OrderSummaryActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
            case R.id.btnBack:
                getActivity().onBackPressed();
                break;
            case R.id.atvCars:
                atvCars.showDropDown();
                atvCars.requestFocus();
                break;
            case R.id.atvNationality:
                atvNationality.showDropDown();
                atvNationality.requestFocus();
                break;

        }
    }

    public class MyClickListener implements AdapterView.OnItemClickListener {

        AutoCompleteTextView ac;

        public MyClickListener(AutoCompleteTextView myAc) {
            ac = myAc;
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            switch (ac.getId()) {
                case R.id.atvCars:
                    for (int j = 0; j < vehiclesDataArrayList.size(); j++) {
                        if (atvCars.getAdapter().getItem(i).toString().equalsIgnoreCase(vehiclesDataArrayList.get(j).getVehicleMakerName())) {
                            car_model_id = vehiclesDataArrayList.get(j).getVehicleMakerId().toString();
                            car_size_id = vehiclesDataArrayList.get(j).getVehicleTypeId().toString();
                            maker_id = vehiclesDataArrayList.get(j).getModelID();
                            id_number = vehiclesDataArrayList.get(j).getIdNumber();
                            owner_name = vehiclesDataArrayList.get(j).getOwnerName();
                            user_phone_number = vehiclesDataArrayList.get(j).getPhoneNumber();
                            user_plate_number = vehiclesDataArrayList.get(j).getIdNumber();
                            car_size = vehiclesDataArrayList.get(j).getVehicleTypeName();
                            layoutCarDetail.setVisibility(View.VISIBLE);
                            carModelName = vehiclesDataArrayList.get(j).getModelName();
                            etCarModel.setText(vehiclesDataArrayList.get(j).getModelName());
                            etCarName.setText(vehiclesDataArrayList.get(j).getVehicleMakerName());
                            etCarPlate.setText(vehiclesDataArrayList.get(j).getPlateNumber());
                            break;
                        }
                    }
                    break;
                case R.id.atvNationality:

                    break;
            }

        }
    }

    private void getVehiclesData() {
        UiUtils.showLoadingDialog(getContext());
        Call<GetVehiclesResponse> vehiclesResponseCall = apiInterface.getVehicles(String.valueOf(prefUtils.getIntValue(PrefKeys.USER_ID, 0)), prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
        vehiclesResponseCall.enqueue(new Callback<GetVehiclesResponse>() {
            @Override
            public void onResponse(Call<GetVehiclesResponse> call, Response<GetVehiclesResponse> response) {
                UiUtils.hideLoadingDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (!(response.body().getData().isEmpty())) {
                        vehiclesDataArrayList = response.body().getData();
                        for (int i = 0; i < vehiclesDataArrayList.size(); i++) {
                            carsList.add(vehiclesDataArrayList.get(i).getVehicleMakerName());
                        }
                    }
                    vehiclesModel = new ArrayAdapter<String>(getContext(), R.layout.item_drop_down, carsList);
                    atvCars.setAdapter(vehiclesModel);
                } else if (response.errorBody() != null) {
                    // ApiError message = new Gson().fromJson(response.errorBody().charStream(), ApiError.class);
                    Toast.makeText(getActivity(), "error in getting data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetVehiclesResponse> call, Throwable t) {
                UiUtils.hideLoadingDialog();

            }
        });
    }
}
