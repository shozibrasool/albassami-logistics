package com.albassami.logistics.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehiclesData {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("vehicle_maker_id")
    @Expose
    private Integer vehicleMakerId;
    @SerializedName("vehicle_type_id")
    @Expose
    private Integer vehicleTypeId;
    @SerializedName("owner_name")
    @Expose
    private String ownerName;
    @SerializedName("vehicle_type_name")
    @Expose
    private String vehicleTypeName;
    @SerializedName("vehicle_maker_name")
    @Expose
    private String vehicleMakerName;
    @SerializedName("id_number")
    @Expose
    private String idNumber;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("plate_number")
    @Expose
    private String plateNumber;
    @SerializedName("plate_type")
    @Expose
    private String plateType;
    @SerializedName("model_id")
    @Expose
    private String modelID;
    @SerializedName("model_name")
    @Expose
    private String modelName;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("car_type_id")
    @Expose
    private Integer carTypeId;
    @SerializedName("car_type")
    @Expose
    private String carType;
    @SerializedName("car_brand_id")
    @Expose
    private Integer carBrandId;
    @SerializedName("car_brand")
    @Expose
    private String carBrand;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVehicleMakerId() {
        return vehicleMakerId;
    }

    public void setVehicleMakerId(Integer vehicleMakerId) {
        this.vehicleMakerId = vehicleMakerId;
    }

    public Integer getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Integer vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getCarTypeId() {
        return carTypeId;
    }

    public void setCarTypeId(Integer carTypeId) {
        this.carTypeId = carTypeId;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public Integer getCarBrandId() {
        return carBrandId;
    }

    public void setCarBrandId(Integer carBrandId) {
        this.carBrandId = carBrandId;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getVehicleMakerName() {
        return vehicleMakerName;
    }

    public void setVehicleMakerName(String vehicleMakerName) {
        this.vehicleMakerName = vehicleMakerName;
    }

    public String getPlateType() {
        return plateType;
    }

    public void setPlateType(String plateType) {
        this.plateType = plateType;
    }

    public String getModelID() {
        return modelID;
    }

    public void setModelID(String modelID) {
        this.modelID = modelID;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
}
