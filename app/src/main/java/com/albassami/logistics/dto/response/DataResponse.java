package com.albassami.logistics.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DataResponse {
    @SerializedName("regions")
    @Expose
    private ArrayList<RegionBrnaches> regions = null;
    @SerializedName("pricelists")
    @Expose
    private ArrayList<PriceList> pricelists = null;
    @SerializedName("shipment_types")
    @Expose
    private ArrayList<ShipmentType> shipmentTypes = null;

    @SerializedName("car_makers")
    @Expose
    private ArrayList<CarMaker> carMakers = null;


    public ArrayList<CarMaker> getCarMakers() {
        return carMakers;
    }

    public void setCarMakers(ArrayList<CarMaker> carMakers) {
        this.carMakers = carMakers;
    }

    public ArrayList<PriceList> getPricelists() {
        return pricelists;
    }

    public void setPricelists(ArrayList<PriceList> pricelists) {
        this.pricelists = pricelists;
    }

    public ArrayList<ShipmentType> getShipmentTypes() {
        return shipmentTypes;
    }

    public void setShipmentTypes(ArrayList<ShipmentType> shipmentTypes) {
        this.shipmentTypes = shipmentTypes;
    }

    public ArrayList<RegionBrnaches> getRegions() {
        return regions;
    }

    public void setRegions(ArrayList<RegionBrnaches> regions) {
        this.regions = regions;
    }

}
