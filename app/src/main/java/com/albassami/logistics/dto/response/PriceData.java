package com.albassami.logistics.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PriceData {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("customer_type")
    @Expose
    private String customerType;

    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("addtional_price")
    @Expose
    private Double addtionalPrice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getAddtionalPrice() {
        return addtionalPrice;
    }

    public void setAddtionalPrice(Double addtionalPrice) {
        this.addtionalPrice = addtionalPrice;
    }
}
